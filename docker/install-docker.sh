#bash

#卸載舊版
sudo apt-get remove docker docker-engine docker.io containerd runc

#更新 apt 包索引。
sudo apt update

#安裝 apt 依賴包，用於通過HTTPS來獲取倉庫: 
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y

#添加 Docker 的官方 GPG 密鑰： 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#設置穩定版倉庫 
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

##查看docker-ce安裝版本
sudo apt-cache policy docker-ce

#安裝docker
sudo apt install docker-ce -y

#修改使用者帳號
sudo usermod -aG docker $USER

#顯示docker 版本
sudo docker -version

#安装 docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

#變更 docker-compose權限
sudo chmod +x /usr/local/bin/docker-compose

#顯示docker-compose  版本
sudo docker-compose -v

#重開機
sudo reboot