package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.practice/internal/domain"
)

//在 main 外面宣告並賦值（全域變數）
var (
	servicePort = ":8000"
)

func main() {

	r := gin.Default()

	//health check
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "PONG")
	})

	r.POST("/activation", domain.FormatActivateReq())

	r.Run(servicePort) //啟動server監聽port，port須包含 ":""
}
