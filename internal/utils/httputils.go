package utils

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

func initHttpClient(certFile string, keyFile string) *http.Client {
	// Load client cert
	fmt.Printf("certFile path= %s, keyFile path=%s \n", certFile, keyFile)
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		fmt.Println(err)
	}

	//tls.Config setting
	tlsConfig := &tls.Config{
		Certificates:  []tls.Certificate{cert},
		Renegotiation: tls.RenegotiateOnceAsClient, // 因local error: tls: no renegotiation, 故加上 tls.RenegotiateOnceAsClient
	}

	//Setup HTTPS client
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	c := &http.Client{Transport: transport,
		Timeout: time.Duration(30) * time.Second,
	}
	return c
}

func SendHTTPRequest(httpMethod string, url string, header map[string]string, body string) string {
	c := initHttpClient("/XXX.pem", "/drlive-decrypted.key")

	fmt.Println(fmt.Printf("method=%s, url=%s , body=%s \n", httpMethod, url, body))
	req, err := http.NewRequest(httpMethod, url, strings.NewReader(body))
	if err != nil {
		fmt.Printf("SendHTTPRequest Error= %s \n", err)
	}

	//setup reqeust header
	if len(header) != 0 {
		//set header  e.g. req.Header.Add("Accept", `application/json`)
		for k, v := range header {
			// fmt.Println(fmt.Printf("header %s: %s \n", k, v))
			req.Header.Add(k, v)
		}
	}

	//send request
	resp, err := c.Do(req)
	if err != nil {
		fmt.Printf("Do error=%s \n", err.Error())
		return err.Error()
	}

	//defer 執行的時間點在離開目前的function
	defer resp.Body.Close()
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Do error=%s \n", err.Error())
		return err.Error()
	}

	return string(responseBody)
}
