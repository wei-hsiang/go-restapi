package utils

import (
	"fmt"
	"strings"

	"github.com/antchfx/xmlquery" //parse xml教學 :  https://github.com/antchfx/xpath、https://github.com/antchfx/xmlquery
)

func ParseXMLStr(tagName string, xmlStr string) map[string]string {

	doc, err := xmlquery.Parse(strings.NewReader(xmlStr))
	if err != nil {
		panic(err)
	}
	m := map[string]string{}

	var activationCode = xmlquery.FindOne(doc, "//"+tagName+"/@ActivationCode")
	if activationCode != nil {
		fmt.Printf("activationCode value=%s \n", activationCode.InnerText())
		m["activationCode"] = activationCode.InnerText()
	}

	var errCode = xmlquery.FindOne(doc, "//"+tagName+"Error/@ErrorCode")
	if errCode != nil {

		fmt.Printf("errCode value=%s \n", errCode.InnerText())
		m["errCode"] = errCode.InnerText()

		var errMsg = xmlquery.FindOne(doc, "//"+tagName+"Error/@ErrorMessage")
		fmt.Printf("errMsg value=%s \n", errMsg.InnerText())
		m["errMsg"] = errMsg.InnerText()
	}

	return m
}
