package models

type Config struct {
	URL       string
	NameSpace string
	UserName  string
	Password  string
}
