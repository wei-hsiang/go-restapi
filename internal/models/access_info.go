package models

type AccessInfo struct {
	Xmlns    string `xml:"xmlns,attr"`
	UserName string
	Password string
}
