package models

import "encoding/xml"

type SoapRoot struct {
	XMLName xml.Name `xml:"soapenv:Envelope"`
	X       string   `xml:"xmlns:soapenv,attr"`
	Header  SoapHeader
	Body    SoapBody
}

type SoapHeader struct {
	XMLName    xml.Name `xml:"soapenv:Header"`
	AccessInfo AccessInfo
}

type SoapBody struct {
	XMLName                      xml.Name `xml:"soapenv:Body"`
	SubscriptionRequestContainer SubscriptionRequestContainer
}
