package models

type TransactionError struct {
	ErrorCode    string `json:"ErrorCode" validate:"required"`
	ErrorMessage string `json:"ErrorMessage" validate:"required"`
}
