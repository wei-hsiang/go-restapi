package models

type SubscriptionRequestContainer struct {
	Xmlns               string `xml:"xmlns,attr"`
	Timestamp           string
	TransactionId       string
	SubscriptionRequest SubscriptionRequest
}
