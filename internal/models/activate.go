package models

type Activate struct {
	UnitId       string `json:"UnitId" validate:"required" xml:"UnitId,attr"`
	SubscriberId string `json:"SubscriberId" validate:"required" xml:"SubscriberId,attr"`
	StartTime    string `json:"StartTime" validate:"required" xml:"StartTime,attr"`
	EndTime      string `json:"EndTime" validate:"required" xml:"EndTime,attr"`
	LicenseCount string `json:"LicenseCount" validate:"required" xml:"LicenseCount,attr"`
	ProductId    string `json:"ProductId" validate:"required" xml:"ProductId,attr"`
}

type ActivateError struct {
	SubscriberId string `json:"SubscriberId" validate:"required"`
	ErrorCode    string `json:"StartTime" validate:"required"`
	ErrorMessage string `json:"EndTime" validate:"required"`
}
