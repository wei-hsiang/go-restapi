package domain

import (
	// "encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"time"

	"go.practice/internal/models"
	"go.practice/internal/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/spf13/viper"
)

// use a single instance of Validate, it caches struct info
var validate *validator.Validate
var configuration models.Config

//全域變數
const (
	requestURI = "https://server:port/soap/upi"
)

//init() 可以在同一個 package 內宣告多次, 且在 func main  之前執行
func init() {
	viper.SetConfigName("config")   // name of config file (without extension)
	viper.SetConfigType("json")     // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("./config") // optionally look for config in the working directory
	// viper.SetEnvPrefix("test")   // will be uppercased automatically

	// var configuration models.Config
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %w \n", err))
	}

	unmarshalErr := viper.Unmarshal(&configuration)
	if unmarshalErr != nil {
		fmt.Printf("Unable to decode into struct, %v", unmarshalErr)
	}
}

//Go提供两種訪問級別 private、public
//fun name 首字大寫代表public(才能夠被外部存取)
func FormatActivateReq() gin.HandlerFunc {
	fn := func(c *gin.Context) {

		//& 是取地址符號 , 即取得某個變量的[地址], 如 &a
		//* 是指針運算符 , 可以表示一個變量是指針類型, 也可以表示一個指針變量所指向的存儲單元, 也就是這個地址所存儲的[值]

		// var m map[string]interface{}
		// err := c.Bind(&m)
		// if err != nil {
		// 	c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		// 	return
		// }
		// fmt.Printf("%v\n", m)
		// c.Bind(&activate)
		// c.JSON(http.StatusOK, "success")

		var activate models.Activate
		err := c.BindJSON(&activate)
		if err != nil {
			fmt.Printf("BindJSON error=%s \n", err)
			return
		}
		fmt.Println("bind to Activate struct success")

		//驗證 request body
		validate = validator.New()
		validateErr := validate.Struct(activate)
		if validateErr != nil {
			for _, validateErr := range validateErr.(validator.ValidationErrors) {
				// can differ when a custom TagNameFunc is registered or  by passing alt name to ReportError like below
				fmt.Println(validateErr.StructNamespace() + " is null or empty")
			}
			return
		}

		//setup struct
		subscriptionRequest := models.SubscriptionRequest{
			Activate: activate,
		}
		root := setRequestSoapRoot(subscriptionRequest)

		//convert struct format to string xml
		output, xmlErr := xml.MarshalIndent(root, " ", "  ")
		if xmlErr != nil {
			fmt.Println(xmlErr)
			c.JSON(http.StatusOK, xmlErr)
			return
		}
		fmt.Println(string(output))

		m := map[string]string{
			"Content-Type": "application/xml",
		}
		responseStr := utils.SendHTTPRequest("POST", configuration.URL, m, string(output))

		respMap := utils.ParseXMLStr("Activate", responseStr)

		value, isExist := respMap["activationCode"]
		if isExist {
			c.JSON(http.StatusOK, value)
		} else {
			errMsg := models.TransactionError{
				ErrorCode:    respMap["errCode"],
				ErrorMessage: respMap["errMsg"],
			}
			c.JSON(http.StatusOK, errMsg)
		}
	}
	return gin.HandlerFunc(fn)
}

func setRequestSoapRoot(subscriptionRequest models.SubscriptionRequest) models.SoapRoot {

	now := time.Now().UTC().Format("2006-01-02T15:04:05Z")

	subscriptionRequestContainer := models.SubscriptionRequestContainer{
		Xmlns:               configuration.NameSpace,
		Timestamp:           now,
		TransactionId:       uuid.New().String(),
		SubscriptionRequest: subscriptionRequest,
	}

	accessInfo := models.AccessInfo{
		Xmlns:    configuration.NameSpace,
		UserName: configuration.UserName,
		Password: configuration.Password,
	}

	var header = models.SoapHeader{}
	header.AccessInfo = accessInfo

	var body = models.SoapBody{}
	body.SubscriptionRequestContainer = subscriptionRequestContainer

	var root = models.SoapRoot{}
	root.X = "http://schemas.xmlsoap.org/soap/envelope/"
	root.Header = header
	root.Body = body

	return root
}
