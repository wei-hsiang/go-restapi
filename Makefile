APP_NAME=restapi

# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=restapi
BINARY_UNIX=$(BINARY_NAME)_unix

# swag-init:
# 	swag init -d ./ -g cmd/main.go --parseInternal=true

init:
	rm -f go.mod go.sum
	go mod init $(APP_NAME)
	go get -d ./...
	#go mod tidy

build:
	rm -rf ./.bin ./.tmp
	mkdir -p ./.bin ./.tmp
	go build -o ./.bin/main ./cmd/main.go
run:
	./.bin/main
clean:
	sh ./scripts/dev-clean.sh
dev-up:
	docker-compose -f air/docker-compose.yaml up -d --force-recreate
	docker-compose -f air/docker-compose.yaml logs -f --tail=30   